import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;


public class ZillowSearch implements Runnable{
	
	public WebDriver driver;
	public String url="http://www.zillow.com";
	public String searchURL="http://www.zillow.com/homes/query_rb/";
	public String email="sscrackers1@gmail.com";
	public String password="140@Broadway";
	public String inputfilename="input.csv";
	public String outputfilename="output.csv";
	
	
	ProgressBar progressbar;
    Label counterLabel;
    Label totalLabel;
    Label percentage;
    Button Button;
    int browser=0;
	
    public ZillowSearch(String input, String output, String username, String password, int browser, ProgressBar bar, Label cLabel, Label percentage, Label tLabel,Button b ) 
    {
    	email=username;
    	this.password=password;
    	inputfilename=input;
    	outputfilename=output;
    	this.browser=browser;
    	progressbar=bar;
    	counterLabel=cLabel;
    	this.percentage=percentage;
    	Button=b;
    	totalLabel=tLabel;
    }
    
    
	public void loadDrivers(){
		print("Loading Browser");
		if(browser == 1){
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			driver= new ChromeDriver();
		}else{
			driver= new FirefoxDriver();			
		}
		print("Browser is Ready");
	}
	
	 public static void getDirectory(String name){
			File theDir = new File(name);
			// if the directory does not exist, create it
			  if (!theDir.exists())
			  { theDir.mkdir();  
				    
			  }
		}
	 public static String lograndom=random();
	 
	 public static String random(){
			Integer d = Integer.valueOf((int) (Math.random() *100000000));
			return Integer.toString( d);
		}
	
	
	public void writeText(String s){ 
		try {  
				
				FileWriter fWriter = null;
				BufferedWriter writer = null; 
				getDirectory("SessionLogs");
				fWriter = new FileWriter("SessionLogs"+"\\"+"sessionslog_"+lograndom+".txt", true);
				writer = new BufferedWriter(fWriter);
				writer.append(s);
				writer.newLine();
				writer.close(); 

		} catch (Exception e) {
		}
		
		
	}
	
	
	
	public void print(String s){
		System.out.println(s);
		writeText(s);
	}
	
	public void signIn(){
		try{
			print("Loading URL");
			driver.get(url);
			print("Signing In");
			String subUrl = driver.findElement(By.id("login_opener")).getAttribute("href");
			driver.get(subUrl);
			print("Waiting for Email Box");
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("emailAddr")));
			print("Email Box Found");
			driver.findElement(By.id("emailAddr")).sendKeys(email);
			driver.findElement(By.id("password")).sendKeys(password);
			driver.findElement(By.id("login_submit")).click();
			print("Clicked Sign In");
			
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("span.menu-label")));
			print("Sign in Successful");	
			
			
		}catch(Exception e){
			print("Error: During Sign in");
			e.printStackTrace();
		}
		
	}
	
	public String getValue(Element mainStatusBlock){
		try{
				String value=mainStatusBlock.getElementsByClass("value").first().text();
				print(value);
				return value;
		}
		catch(Exception e){
			return "";
		}
	}
	
	public void mainStatus(Map<String,String> datamap,Element mainStatusBlock){
		try{
			String statusType = mainStatusBlock.getElementsByClass("label").first().text();
			try{
				statusType=statusType.replace(":", "");
			}catch(Exception e){}
			
			statusType=statusType.trim();
			print(statusType);
			
			if(statusType.equals("Pre-Foreclosure (Auction)")){
				datamap.put("Status", statusType);
			}
			
			if(statusType.equals("For Sale")){
				datamap.put("Status", statusType);
				datamap.put("Sale Price", getValue(mainStatusBlock));
			}
			
			if(statusType.equals("For Rent")){
				datamap.put("Status", statusType);
				datamap.put("Rent Price",getValue(mainStatusBlock));
			}
			
			if(statusType.contains("Sold on")){
				datamap.put("Status", "Sold");
				try{
					String date= statusType.substring(statusType.lastIndexOf(" "));
					date=date.trim();
					datamap.put("Sold Date", date);
				}catch(Exception e){}
				datamap.put("Sold Value", getValue(mainStatusBlock));
			}
			
			if(statusType.equals("Not for Sale")){
				datamap.put("Status", "Not for Sale");
			}
			
			if(statusType.equals("Foreclosed")){
				datamap.put("Status", "Foreclosed");
			}
			
			
			
		}catch(Exception e){}
	}
	
	public void extractInfo(String html,Map<String,String> datamap){
		try{
			Document doc= Jsoup.parse(html);
			String address="";
			try{
				address=address+doc.select("h1.prop-addr").first().text();
			}catch(Exception e){}
			try{
				address=address+ doc.select("span.prop-addr-unit").first().text();
			}catch(Exception e){}
			
			try{
				address=address+ doc.select("span.prop-addr-city").first().text();
			}catch(Exception e){}
			datamap.put("Found Address"	, address);
			
			Element priceWrapper = doc.getElementsByClass("prop-value-price-wrapper").first();
			Element mainStatusBlock = priceWrapper.getElementsByTag("h2").first();
			mainStatus(datamap, mainStatusBlock);
			
			try{
				Elements secondryValues = priceWrapper.getElementsByClass("secondary-val");
				for(Element secondryValue : secondryValues){
					try{
						String label = secondryValue.getElementsByClass("label").first().text();
						print(label);
						try{
							label=label.replace(":", "");
							label=label.replace("®", "");
						}catch(Exception e){}
						
						label=label.trim();
						if(label.equals("Est. Mortgage")){
							String temp = secondryValue.select("span#monthlyPaymentAmount").first().text();
							temp=temp+secondryValue.select("span.de-emph").first().text();
							datamap.put(label, temp);
						}else{
							datamap.put(label, getValue(secondryValue));
						}
					}catch(Exception e){}

				}
			}catch(Exception e){}
			
			
			//property facts
			try{
				Elements propFactsLabels = doc.getElementsByClass("prop-facts-label");
				Elements propFactsValues = doc.getElementsByClass("prop-facts-value");
				int i=0;
				for(Element propFactsLabel : propFactsLabels){
					String label = propFactsLabel.text();
					String value= propFactsValues.get(i).text();
					print( label + value);
					try{
						label=label.replace(":", "");
					}catch(Exception e){}					
					label=label.trim();
					datamap.put(label, value);
					i++;
				}
			
				
			}catch(Exception e){}
			
			
			//writeCSV(datamap);
		}catch(Exception e){
			
		}
	}
	
	public void writeCSV(Map<String,String> datamap){
		boolean alreadyExists = new File(outputfilename).exists();
	    try
			    {
				    CsvWriter csvOutput = new CsvWriter(new FileWriter(outputfilename, true), ',');
				    if (!alreadyExists){
				    	for(int i=0;i<headerList.size();i++){
				    		csvOutput.write(headerList.get(i));
				    	}
				    	csvOutput.endRecord();
				    }
				    
				    for(int i=0;i<headerList.size();i++){
				    	try{
				    		csvOutput.write(datamap.get(headerList.get(i)));
				    	}catch(Exception e){
				    		csvOutput.write("");
				    	}
				    }
				    csvOutput.endRecord();
				 
				    csvOutput.close();
				    print("Data Successfully Inserted");
			    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
	}
	
	
	public void search(String query, Map<String,String> datamap){
		try{
			
			String searchURI = searchURL.replaceAll("query", query);
			driver.get(searchURI);
			try{
				
				String resText="";
				try{
					new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("info-message-bar")));
					resText = driver.findElement(By.id("info-message-bar")).getText();
					if(resText.contains("No matching homes")){
						print("No Results Found");
					}
					else{
					String html = driver.getPageSource();
					Document doc = Jsoup.parse(html);
					Element link= doc.select("a.hdp-link.routable").first();
					String propertyURL = link.attr("href");
						
					driver.get(url+propertyURL);
					datamap.put("URL", url+propertyURL);
					String html2=driver.getPageSource();
					extractInfo(html2,datamap);
					
					writeCSV(datamap);
					}
				}catch(Exception e){
					String html=driver.getPageSource();
					Document doc=Jsoup.parse(html);
					Element link=doc.select("a.inline-button.button-alt-color.hc-pop-out.track-ga-event").first();
					String propertyURL = link.attr("href");
					
					driver.get(url+propertyURL);
					datamap.put("URL", url+propertyURL);
					String html2=driver.getPageSource();
					extractInfo(html2,datamap);
					
					writeCSV(datamap);
					
				}
				
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	ArrayList<String> headerList = new ArrayList<String>();
	
	String[] newheaders={"ZilScrapeDate","ZilFound Address","ZilStatus", "ZilSale Price","ZilSold Date","ZilSold Value","ZilRent Price","ZilForeclosure Est","ZilBelow Zestimate","ZilZestimate","ZilEst. Mortgage","ZilRent Zestimate","ZilDeposit & Fees","ZilEst. Refi Payment","ZilBedrooms","ZilBathrooms","ZilYear Built","ZilLast Sold","ZilHeating Type","ZilURL"};
	
	public String getDate(){
		try{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String dateString=dateFormat.format(date).toString();
			writeText(dateString);
			return dateString;
		}
		catch(Exception e){
			return "";
		}
	}
	
	
	public void readCSV(){
		readTemp();
		getTotal();
		try{
			print("Reading Input File");
			CsvReader results = new CsvReader(inputfilename);
			results.readHeaders();
			int recordnumber=0;
			String[] headers= results.getHeaders();
			for(int i=0;i<headers.length;i++){
				headerList.add(headers[i]);
			}
			for(int i=0;i<newheaders.length;i++){
				headerList.add(newheaders[i]);
			}
			
			while(results.readRecord()){
				recordnumber++;
				if(recordnumber<=tempcounter){
					print("Record Already Searched");
				}
				else{
					print("================================================================================");
					print("Reading Record # "+recordnumber);
					String dataid=results.get("CLData_ID");
					String address = results.get("CLInput_Address");
					String zip= results.get("ClInput_Zip");
					print("Data ID: "+dataid);
					print("Address: "+address);
					print("Zip: "+zip);
					setCounter(recordnumber);
					String query = address + " "+zip;
					query=query.replaceAll(" ", "-");
					Map<String,String> datamap = new HashMap<String,String>();
					datamap.put("ZilScrapeDate", getDate());
					for(int i=0;i<headers.length;i++){
						datamap.put(headers[i], results.get(headers[i]));
					}
					search(query,datamap);
					writeTemp(dataid);
					print("================================================================================");
				}
			}
			results.close();
			print("THE END");
			setButton();
		}catch(Exception e){
			
		}
	}
	
	public void writeTemp(String dataid){
		boolean alreadyExists = new File("temp.csv").exists();
	    try
			    {
				    CsvWriter csvOutput = new CsvWriter(new FileWriter("temp.csv", true), ',');
				    if (!alreadyExists){
				    	csvOutput.write("data_id");
				    	csvOutput.endRecord();
				    }
				    csvOutput.write(dataid);
				    csvOutput.endRecord();
				    csvOutput.close();
				    print("Data Id inserted into temp");
			
		}
		catch(Exception e){
			
		}
	}
	
	public int tempcounter=0;
	public void readTemp(){
		try{
			CsvReader results = new CsvReader("temp.csv");
			results.readHeaders();
			
			while(results.readRecord()){ 
				tempcounter++;
			}
			print(tempcounter + "Records Already Exists");
			results.close();
		}
		catch(Exception e){
			print("No Records Exists. Starting Fresh");
		}
	}
	
	
	public void run(){
		loadDrivers();
		signIn();
		readCSV();
		
	}
	
	
	int totalRecords=0;  
	  public void getTotal(){
          try{
              CsvReader results = new CsvReader(inputfilename);
              results.readHeaders();
              while(results.readRecord()){
                  totalRecords++;
              }
              writeText("Total Records "+totalRecords);
          }
          catch(Exception e){
          }
      }
	
	
	/*
	 * 
	 * Methods for synchronization with GUI
	 * 
	 **/
	public void setCounter(int c){
    	final int count=c;
    	 Platform.runLater(new Runnable() {
             @Override 
             public void run() {
            	  try{
            		  totalLabel.setText(new Integer(totalRecords).toString());
                      counterLabel.setText(new Integer(count).toString());
                      double progress = new Double(count) * 100 /new Double(totalRecords) ;
                      progressbar.setProgress(progress/100);
                      percentage.setText((Math.round( progress* 100.0 ) / 100.0) + " %");
                  }catch(Exception e){
                      e.printStackTrace();
                  }
             }
           });
      
    }
  
  public void setButton(){
		Platform.runLater(new Runnable() {
            @Override 
            public void run() {
           	  try{
                    Button.setText("Start");
                    Button.setDisable(false);
                 }catch(Exception e){
                     e.printStackTrace();
                 }
            }
          });
	}
	 
	
	

}

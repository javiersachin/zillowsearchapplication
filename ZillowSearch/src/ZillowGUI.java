import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;


public class ZillowGUI implements Initializable{

	@FXML
    private TextField inputText;
     
    @FXML
    private TextField outputText;
    
    @FXML
    private TextField username;
    
    @FXML
    private TextField password;
    
    @FXML
    private Button inputBrowseButton;
    
    @FXML
    private Button outputBrowseButton;
    
    @FXML
    private Button startButton;
    
    @FXML
    public static  Label counter;
    
    @FXML
    public static  Label totalcounter;
    
    @FXML
    public static Label percentage;
    
    @FXML
    private RadioButton chrome;
    
    @FXML
    private RadioButton firefox;
    
    
    @FXML
    private Button save;
    
    @FXML
    public static ProgressBar progress;
    
    
    String user= "";
    String pass = "";
    String browse="";
    String inputloc="";
    String outputloc="";

    int browser ;
    
    @FXML
    private void handleInputBrowseButtonAction(ActionEvent event) {
       FileChooser chooser= new FileChooser(); 
      //  chooser.showOpenDialog(null); 
       chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        File file = chooser.showOpenDialog(null);
     	String inputfile = file.getAbsolutePath(); 
     	inputText.setText(inputfile);
    }
    
    @FXML
    private void handleOutputBrowseButtonAction(ActionEvent event) {
         FileChooser chooser= new FileChooser(); 
       // chooser.showOpenDialog(null); 
        //File file = chooser.getSelectedFile();
         chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
         File file = chooser.showSaveDialog(null);
        String outputfile = file.getAbsolutePath();  
        outputText.setText(outputfile); 
    }
    
    Properties prop;
    public void readConfig(){
         prop = new Properties(); 
        try{ 
            prop.load(new FileInputStream("config.properties")); 
            try{
                user=prop.getProperty("username"); 
                username.setText(user);
            }catch(Exception e){}
            try{
                pass=prop.getProperty("password"); 
                password.setText(pass);
            }catch(Exception e){}             
            try{
            	browse= prop.getProperty("browser");
            	if(browse.equals("1")){
            		firefox.setSelected(true);
            	}
            	if(browse.equals("2")){
            		chrome.setSelected(true);
            	}
            }catch(Exception e){
            	
            }
            try{
            	inputloc=prop.getProperty("input");
            	inputText.setText(inputloc);
            }catch(Exception e){}
            
            try{
            	outputloc=prop.getProperty("output");
            	outputText.setText(outputloc);
            }
            	catch(Exception e){}
            
            
        } 
        catch(Exception e){ 
               
        } 
    }
    
    public void writeConfig(){
        try { 
            //set the properties value 
            prop= new Properties();
            prop.setProperty("username", username.getText().toString()); 
            prop.setProperty("password", password.getText().toString());  
            if(firefox.isSelected()){
            	prop.setProperty("browser","1");
            }
            if(chrome.isSelected()){
            	prop.setProperty("browser","2");
            }
            
               prop.setProperty("input", inputText.getText());
               prop.setProperty("output", outputText.getText());
               
               
            //save properties to project root folder 
            prop.store(new FileOutputStream("config.properties"), null); 
            JOptionPane.showMessageDialog(null, 
                    "Settings Saved Successfully!", 
                    "Application Message", 
                    JOptionPane.INFORMATION_MESSAGE); 
   
        } catch (Exception e) {  
           e.printStackTrace();
        } 
    }
    
        
    @FXML
    private void startButtonAction(ActionEvent event) {
    	startButton.setText("Running");
    	startButton.setDisable(true);
    	inputloc= inputText.getText();
        outputloc= outputText.getText();
        user= username.getText();
        pass = password.getText();
        browser= 2;
        if(chrome.isSelected()){ browser=1;}
        if(firefox.isSelected()){ browser=2;}
        
        try{
            ZillowSearch  app = new ZillowSearch(inputloc,outputloc,user, pass, browser,progress,counter,percentage, totalcounter,startButton);
            Thread t = new Thread(app);
            t.setDaemon(true);
            t.start();
//            app.run();
        }catch(Exception e){}
        
    }
    
    @FXML
    private void chromeAction(ActionEvent event) {
        firefox.setSelected(false);
    }
    
    @FXML
    private void firefoxAction(ActionEvent event) {
        chrome.setSelected(false);
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        readConfig();
    }  
	
}
